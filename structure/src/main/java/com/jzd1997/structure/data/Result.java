package com.jzd1997.structure.data;

import com.sun.org.apache.xpath.internal.operations.Bool;

public class Result {
    public Boolean result;
    public String resultStr;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getResultStr() {
        return resultStr;
    }

    public void setResultStr(String resultStr) {
        this.resultStr = resultStr;
    }

}
