package com.jzd1997.structure.controller;

import com.jzd1997.structure.data.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jzd1997.structure.service.IMainService;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RestController
public class MainController {
	@Autowired
	IMainService mainService;
	
	@GetMapping("/create")
	public Result create(HttpServletResponse response) {
		Result res = new Result();
		try{
			res.result = mainService.findStructure(response);
			res.resultStr = "done";
			return res;
		}catch (Exception e){
			res.result = false;
			res.resultStr = e.toString();
			return res;
		}
	}
}
