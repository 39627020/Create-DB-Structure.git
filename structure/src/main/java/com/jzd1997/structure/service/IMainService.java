package com.jzd1997.structure.service;

import javax.servlet.http.HttpServletResponse;

public interface IMainService {
	boolean findStructure(HttpServletResponse response) throws Exception;
}
